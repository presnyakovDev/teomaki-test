import { Component, OnInit } from '@angular/core';
import { MockCountriesService } from '../../services/countries.service';
import { Router } from '@angular/router';
import Country from '../../types/Country';

@Component({
  selector: 'app-country-selector',
  templateUrl: './country-selector.component.html',
  styleUrls: ['./country-selector.component.scss']
})
export class CountrySelectorComponent implements OnInit {
  countrties: Country[] = [];

  active_country_url;

  constructor(
    private countriesService: MockCountriesService,
    private router: Router) {
  }

  ngOnInit() {
    this.countriesService.GetCountries()
      .subscribe((res: Array<Country>) => {
        this.countrties = res
      });
  }

  onChange(target) {
    this.router.navigate(['/forecast/' + target]);
  }

}
