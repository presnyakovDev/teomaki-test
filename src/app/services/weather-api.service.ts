import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import Weather from '../types/Weather';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MockWeatherApiService {

  constructor(private http: HttpClient) { }

  public GetWeather(country:string):Observable<Weather[]>{
    return this.http.get<Weather[]>('./assets/weather.json').pipe(
      map((res: Array<Weather>) => res.filter(city => city.country === country))
    )
  }
}
