import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import Country from '../types/Country';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MockCountriesService {

  constructor(private http: HttpClient) { }

  public GetCountries():Observable<Country[]>{
    return this.http.get<Country[]>('./assets/countries.json')
  }
}
