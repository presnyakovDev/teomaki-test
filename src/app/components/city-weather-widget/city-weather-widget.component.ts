import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-city-weather-widget',
  templateUrl: './city-weather-widget.component.html',
  styleUrls: ['./city-weather-widget.component.scss']
})
export class CityWeatherWidgetComponent implements OnInit {
  @Input() cityName: string;
  @Input() temperature: Number;
  @Input() wind: Number;
  @Input() pressure: Number;
  @Input() precipitation: Number;
  constructor() { }

  ngOnInit() {
  }

}
