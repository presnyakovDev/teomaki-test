import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MockWeatherApiService } from '../../services/weather-api.service';
import { Router, NavigationEnd, RouterEvent } from '@angular/router';
import Weather from '../../types/Weather';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-weather-widgets',
  templateUrl: './weather-widgets.component.html',
  styleUrls: ['./weather-widgets.component.scss']
})
export class WeatherWidgetsComponent implements OnInit {
  cities: Weather[] = [];

  constructor(
    private weatherApiService: MockWeatherApiService,
    private activateRoute: ActivatedRoute,
    private router: Router)
    {
      this.activateRoute.paramMap.pipe(
        filter(res => res.has('country_url'))
      ).subscribe((res) => {
        let country_url = this.activateRoute.snapshot.paramMap.get('country_url');
        this.weatherApiService.GetWeather(country_url)
          .subscribe(res => {
            this.cities = res
          })
      });
    }

  ngOnInit() {

  }

}
