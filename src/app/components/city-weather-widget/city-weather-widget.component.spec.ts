import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CityWeatherWidgetComponent } from './city-weather-widget.component';

describe('CityWeatherWidgetComponent', () => {
  let component: CityWeatherWidgetComponent;
  let fixture: ComponentFixture<CityWeatherWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CityWeatherWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CityWeatherWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
