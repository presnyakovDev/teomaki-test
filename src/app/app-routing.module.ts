import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WeatherWidgetsComponent } from './components/weather-widgets/weather-widgets.component';

const routes: Routes = [
  { path: "forecast/:country_url", component: WeatherWidgetsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
