import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { CountrySelectorComponent } from './components/country-selector/country-selector.component';
import { WeatherWidgetsComponent } from './components/weather-widgets/weather-widgets.component';
import { CityWeatherWidgetComponent } from './components/city-weather-widget/city-weather-widget.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    CountrySelectorComponent,
    WeatherWidgetsComponent,
    CityWeatherWidgetComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [HttpClientModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
