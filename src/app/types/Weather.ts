interface Weather{
  cityName: string;
  temperature: number;
  wind: number;
  pressure: number;
  precipitation: number;
  country: string;
}

export default Weather;
